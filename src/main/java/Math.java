public class Math {
    public int divide(int x, int y) {
        if (x < 0 || y < 0) {
            throw new IllegalArgumentException("X Y should be not less than 0");
        }
        return x / y;
    }

    protected int sum(int x, int y) {
        return x + y;
    }
}