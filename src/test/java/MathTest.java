import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class MathTest {
    private Math math;

    @Before
    public void setUp() throws Exception {
        math = new Math();
    }

    @After
    public void tearDown() throws Exception {
        math = null;
    }

    @Test(expected = IllegalArgumentException.class)
    public void divide() {
        math.divide(-5, -4);
    }

    @Test(expected = Exception.class)
    public void divide0() {
        math.divide(5, 0);
    }
    @Test
    public void divideN() {
       assertEquals(2, math.divide(10,5));
    }

    @Test
    public void sum() {
        assertTrue (math.sum(5,5)==10);
    }
}